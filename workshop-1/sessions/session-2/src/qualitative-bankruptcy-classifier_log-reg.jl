println("... welcome to my binomial classifier using logistic regression....")

println("importing packages....")
using DataFrames, CSV, PrettyTables, Missings, Plots, StatsPlots, Statistics

#this function prepares the data and returns the training and testing features and outcome
function prepare_dataset()
	# load the data as a matrix
	println("loading the dataset into a matrix...")
	all_data = convert(Matrix, CSV.read("../data/qualitative-bankruptcy.csv"))
	pretty_table(all_data, tf = borderless, noheader = false)

	#extract matrix size
	mat_row_count = size(all_data)[1]
	mat_col_count = size(all_data)[2]

	println("The number of rows of the matrix is: ", mat_row_count)
	println("The number of columns of the matrix is: ", mat_col_count)

	#extract the outcome vector
	println("extracting the outcome vector...")
	raw_outcome = all_data[:, 7]
	pretty_table(raw_outcome, tf = borderless, noheader = true)

	revised_outcome = zeros(Int64,mat_row_count)
	println("initialised revised outcome looks as follows")
	pretty_table(revised_outcome, tf = borderless, noheader = true)

	for i in 1:mat_row_count
		if raw_outcome[i] == "NB"
			revised_outcome[i] = 1
		end
	end
	println("revised outcome now looks as follows:")
	pretty_table(revised_outcome, tf = borderless, noheader = true)

	#extract the features
	println("extracting the features now...")
	raw_features = all_data[:, 1:6]
	pretty_table(raw_features, tf = borderless, noheader = true)

	feature_mat_col_count = mat_col_count - 1

	revised_features = zeros(Int64, mat_row_count, feature_mat_col_count)
	println("initialised revised features look as follows")
	pretty_table(revised_features, tf = borderless, noheader = true)

	for i in 1:mat_row_count
		for j in 1:feature_mat_col_count
			if raw_features[i,j] == "P"
				revised_features[i,j] = 1
			elseif raw_features[i,j] == "N"
				revised_features[i,j] = -1
			end
		end
	end

	println("revised features now look as follows:")
	pretty_table(revised_features, tf = borderless, noheader = true)

	#extract training and testing data set
	train_index = trunc(Int, 0.7 * mat_row_count)


	x_train = revised_features[1:train_index,:]
	x_test = revised_features[train_index + 1:end,:]

	y_train = revised_outcome[1:train_index,:]
	y_test = revised_outcome[train_index + 1:end,:]

	println("the new matrices look as follows...")
	pretty_table(x_train, tf = borderless, noheader = true)
	pretty_table(x_test, tf = borderless, noheader = true)
	pretty_table(y_train, tf = borderless, noheader = true)
	pretty_table(y_test, tf = borderless, noheader = true)
	
	return (x_train, y_train, x_test, y_test)
end


#This function computes the sigmoid to an expression
function sigmoid(zed)
	return 1 ./ (1 .+ exp.(.- zed))
end

#this function evaluates the average loss and regularises it
#it returns the cost and the gradient
function regularised_loss(features, outcome, weights, lambda)
	m = length(outcome)
	aggregated_input = features * weights
	hypothesis = sigmoid(aggregated_input)
	
	loss_part_one = ((.- outcome)' * log.(hypothesis))
	loss_part_two = ((1 .- outcome)' * log.(1 .- hypothesis))
	
	println("printing out the loss_part_one...", loss_part_one)
	println("printing out the loss_part_two...", loss_part_two)
	
	lambda_regularization = (lambda/(2*m) * sum(weights[2 : end] .^ 2))

	regularised_cost = (1/m) .* (loss_part_one .- loss_part_two) .+ lambda_regularization
	println("printing out the regularised cost...", regularised_cost)

	grad = (1/m) .* (features') * (hypothesis - outcome) + ((1/m) .* (lambda * weights))
	grad[1] = ((1/m) * (features[:, 1])' * (hypothesis - outcome))[1]
	println("printing out the grad...", grad)
	
	return (regularised_cost[1], grad)
end

function logistic_reg(features, outcome, lambda, learning_rate=0.01, max_iteration=500)
	println("running the logistic regression algorithm...")
	
	m = length(outcome)
	
	bias_vec = ones(m,1)
	augmented_features = hcat(bias_vec, features)
	
	feature_count = size(augmented_features)[2]
	weight_vec = zeros(feature_count)
	cost_vec = zeros(max_iteration)
	
	# gradient descent implementation
	
	for i in range(1, stop=max_iteration)
		println("iteration round ", i)
		cost_vec[i], grad = regularised_loss(augmented_features, outcome, weight_vec, lambda)
		println("printing out the cost vect...", cost_vec[1])
		
		#update the weight
		weight_vec = weight_vec - (learning_rate * grad)
	end
	
	return (weight_vec, cost_vec)
end

function predict(features, weights, threshold=0.5)
    m = size(features)[1]
	bias_vec = ones(m,1)
	augmented_features = hcat(bias_vec, features)
    hypothesis = sigmoid(augmented_features * weights)
    return hypothesis .> threshold
end

function confussion_matrix(predictions, labels)
   c = zeros(Int64, 2,2)
   for i in 1:length(labels)
		c[labels[i] + 1 , predictions[i] + 1] += 1 
   end 
   return c
end

function train_and_test()
	#	get the dataset ready
	println("will start preparing for the training and tesing dataset...")
	training_features, training_outcome, testing_features, testing_outcome = prepare_dataset()
	
	all_weights, all_costs = logistic_reg(training_features, training_outcome, 0.0001, 0.3, 700)
	plot(all_costs, color="green", title="Cost Per Iteration", legend=false, xlabel="Num of iterations", ylabel="Cost")
	savefig("./cost.png")
	
	
	train_score = mean(training_outcome .== predict(training_features, all_weights));
	test_score = mean(testing_outcome .== predict(testing_features, all_weights));

	println("Training score: ", round(train_score, sigdigits=4))
	println("Testing score: ", round(test_score, sigdigits=4))
	
	println("printing hte confusion matrix...")
	conf_mat = confussion_matrix(predict(testing_features, all_weights), testing_outcome)
	display(conf_mat)
end

train_and_test()